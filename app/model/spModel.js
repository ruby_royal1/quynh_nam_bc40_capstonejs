const BASE_URL = "https://63c544bce1292e5bea1f15f1.mockapi.io/";
function sanPham() {
    this.layDSSP = function () {
        // Lấy dữ liệu
        var pro = axios({
            method: 'get',
            url: `${BASE_URL}/product_mng`,

        });

        return pro;
    }
    this.themSP = function (user) {
        //POST: Thêm mới dữ liệu
        //data: dữ liệu cần thêm vào Cơ sở dữ liệu
        var pro = axios({
            method: 'post',
            url: `${BASE_URL}/product_mng`,
            data: user
        });

        return pro;
    }
    this.xoaSP = function (id) {
        //DELETE: xóa sản phẩm dựa vào id
        var pro = axios({
            method: 'delete',
            url: `${BASE_URL}/product_mng/${id}`
        });

        return pro;
    }
    this.xemSP = function (id) {
        //GET: lấy data cua 1 sản phẩm dựa vào id
        var pro = axios({
            method: 'get',
            url: `${BASE_URL}/product_mng/${id}`
        });

        return pro;
    }

    this.updateSP = function (id, user) {
        //PUT: cập nhật data của 1 sản phẩm dựa vào id
        var pro = axios({
            method: 'put',
            url: `${BASE_URL}/product_mng/${id}`,
            data: user
        });

        return pro;
    }

    this.searchUser = function(mangUser, chuoiTK){

        var mangTK = [];
       mangTK = mangUser.filter(function(user){
            return user.tenSP.toLowerCase().indexOf(chuoiTK.toLowerCase()) >= 0;
        });
        return mangTK;
    }
}
