function getFormInfo() {
  var name = getEl("txt-name").value;
  var price = getEl("txt-price").value;
  var screen = getEl("txt-screen").value;
  var backCamera = getEl("txt-backCamera").value;
  var frontCamera = getEl("txt-frontCamera").value;
  var img = getEl("txt-img").value;
  var desc = getEl("txt-desc").value;
  var type = getEl("txt-type").value;


  var sp = { name, price, screen, backCamera, frontCamera, img, desc, type };
  return sp;
}

function convertString(maxLength, value) {
  if (value.length > maxLength) {
    return value.slice(0, maxLength) + "...";
  } else {
    return value;
  }
}