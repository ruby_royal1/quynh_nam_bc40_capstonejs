var spObj = new sanPham();
// rút gọn lấy dữ liệu
function getEl(id) {
  return document.getElementById(id);

}
getListSP();

function getListSP() {
  spObj.layDSSP()
    .then(function (result) {
      console.log(result.data);
      renderItem(result.data);
      setLocalStorage(result.data);
    })
    .catch((err) => {
      console.log(err);
    });
  function setLocalStorage(spArr) {
    localStorage.setItem("DSSP", JSON.stringify(spArr));
  }
}


// Lấy dữ liệu từ localstorage
function getLocalStorage() {
  var resultArr = JSON.parse(localStorage.getItem("DSSP"));
  return resultArr
}

// Lấy dữ liệu từ local storage
var productDetails = getLocalStorage();
// Render item ra view


// Xóa item card
function removeItem(e) {
  let t = e.parentElement.getElementsByClassName("name")[0].innerText,
    n = document.getElementsByClassName("product-name");
  cartDetails.forEach((e, a) => {
    if (t == e.name) {
      cartDetails.splice(a, 1);
      for (let e of n)
        if (t == e.innerText) {
          SwitchBtns(e.parentElement.parentElement)
        }
    }
  }), RenderCart(), CartIsEmpty(), CartItemsTotal()
}





// Thêm mặt hàng
function addItem(e) {
  let t = e.parentElement.parentElement.parentElement.parentElement.parentElement;
  if ("flex" == t.getElementsByClassName("out-of-stock-cover")[0].style.display) return;
  let n = t.getElementsByClassName("product-name")[0].innerText,
    a = parseFloat(t.getElementsByClassName("product-price")[0].innerText.replace("$ ", "")),
    s = t.getElementsByClassName("product-img")[0].src;
  SwitchBtns(t);
  let i = {
    name: n,
    price: a,
    imgSrc: s,
    qty: 1
  };
  CartItems(i), cartDetails.push(i), RenderCart(), CartItemsTotal()
}



function clearCart() {
  ToggleBackBtns(), cartDetails.length = 0, RenderCart(), CartIsEmpty(), CartItemsTotal()
}

function qtyChange(e, t) {
  let n = e.parentElement.parentElement,
    a = n.classList.contains("btn-add") ? n.parentElement.parentElement.getElementsByClassName("product-name")[0].innerText : n.parentElement.getElementsByClassName("name")[0].innerText,
    s = document.getElementsByClassName("product-name");
  for (let e of s)
    if (a == e.innerText) {
      let s = e.parentElement.parentElement.getElementsByClassName("qty-change")[0];
      cartDetails.forEach((e, i) => {
        a == e.name && ("add" == t && e.qty < 10 ? (e.qty += 1, n.innerHTML = QtyBtn(e.qty), s.innerHTML = QtyBtn(e.qty)) : "sub" == t ? (e.qty -= 1, n.innerHTML = QtyBtn(e.qty), s.innerHTML = QtyBtn(e.qty), e.qty < 1 && (cartDetails.splice(i, 1), s.innerHTML = AddBtn(), s.classList.toggle("qty-change"))) : (document.getElementsByClassName("purchase-cover")[0].style.display = "block", document.getElementsByClassName("stock-limit")[0].style.display = "flex", sideNav(0)))
      })
    } RenderCart(), CartIsEmpty(), CartItemsTotal()
}

function limitPurchase(e) {
  document.getElementsByClassName("purchase-cover")[0].style.display = "none", e.parentElement.style.display = "none", sideNav(1)
}

function sideNav(e) {
  let t = document.getElementsByClassName("side-nav")[0],
    n = document.getElementsByClassName("cover")[0];
  t.style.right = e ? "0" : "-100%", n.style.display = e ? "block" : "none", CartIsEmpty()
}

function buy(e) {
  0 != cartDetails.length && (sideNav(!e), document.getElementsByClassName("purchase-cover")[0].style.display = e ? "block" : "none", document.getElementsByClassName("order-now")[0].innerHTML = e ? Purchase() : "")
}

function order() {
  let e = document.getElementsByClassName("invoice")[0];
  e.style.height = "500px", e.style.width = "400px", e.innerHTML = OrderConfirm(), ToggleBackBtns(), Stocks(), clearCart()
}

function okay(e) {
  let t = document.getElementsByClassName("invoice")[0];
  "continue" == e.target.innerText ? (t.style.display = "none", document.getElementsByClassName("purchase-cover")[0].style.display = "none") : (e.target.innerText = "continue", e.target.parentElement.getElementsByClassName("order-details")[0].innerHTML = "<em class='thanks'>Thanks for shopping with us</em>", t.style.height = "180px")
}

function AddBtn() {
  return "\n  <div>\n<button onclick='addItem(this)' class='add-btn'>Add <i class='fas fa-chevron-right'></i></button>\n  </div>"
}

function QtyBtn(e = 1) {
  return 0 == e ? AddBtn() : `\n  <div>\n<button class='btn-qty' onclick="qtyChange(this,'sub')"><i class='fas fa-chevron-left'></i></button>\n<p class='qty'>${e}</p>\n<button class='btn-qty' onclick="qtyChange(this,'add')"><i class='fas fa-chevron-right'></i></button>\n  </div>`
}

function Product(e = {}) {
  let {
    name: t,
    price: n,
    img: a,
    backCamera: s,
    desc: i
  } = e;
  return `\n  <div class='card mb-10'>\n<div class='top-bar'>\n  <i class='fab fa-apple'></i>\n  <em class="stocks">In Stock</em>\n</div>\n<div class='img-container'>\n  <img class='product-img' src='${a}' alt='' />\n  <div class='out-of-stock-cover'><span>Out Of Stock</span></div>\n</div>\n<div class='details'>\n  <div class='name-fav'>\n<strong class='product-name'>${t}</strong>\n<button onclick='this.classList.toggle("fav")' class='heart'><i class='fas fa-heart'></i></button>\n  </div>\n  <div class='wrapper'>\n<h5>${s}</h5>\n<p>${i}</p>\n  </div>\n  <div class='purchase'>\n<p class='product-price'>$ ${n}</p>\n<span class='btn-add'>${AddBtn()}</span>\n  </div>\n</div>\n  </div>`
}

function CartItems(e = {}) {
  let {
    name: t,
    price: n,
    img: a,
    qty: s
  } = e;
  return `\n  <div class='cart-item'>\n<div class='cart-img'>\n  <img src='${a}' alt='' />\n</div>\n<strong class='name'>${t}</strong>\n<span class='qty-change'>${QtyBtn(s)}</span>\n<p class='price'>$ ${n * s}</p>\n<button onclick='removeItem(this)'><i class='fas fa-trash'></i></button>\n  </div>`
}

function Banner() {
  return `\n  <div class='banner'>\n   \n<div class='main-cart'>${DisplayProducts()}</div>\n  \n<div class='nav'>\n  <button onclick='sideNav(1)'><i class='fas fa-shopping-cart' style='font-size:2rem;'></i></button>\n  <span class= 'total-qty'>0</span>\n</div>\n<div onclick='sideNav(0)' class='cover'></div>\n<div class='cover purchase-cover'></div>\n<div class='cart'>${CartSideNav()}</div>\n<div class='stock-limit'>\n  <em>You Can Only Buy 10 Items For Each Product</em>\n  <button class='btn-ok' onclick='limitPurchase(this)'>Okay</button>\n</div>\n  <div  class='order-now'></div>\n  </div>`
}

function CartSideNav() {
  return "\n  <div class='side-nav'>\n<button onclick='sideNav(0)'><i class='fas fa-times'></i></button>\n<h2>Cart</h2>\n<div class='cart-items'></div>\n<div class='final'>\n  <strong>Total: $ <span class='total'>0</span></strong>\n  <div class='action'>\n<button onclick='buy(1)' class='btn buy'>Purchase <i class='fas fa-credit-card' style='color:#6665dd;'></i></button>\n<button onclick='clearCart()' class='btn clear'>Clear Cart <i class='fas fa-trash' style='color:#bb342f;'></i></button>\n  </div>\n</div>\n  </div>"
}

function Purchase() {
  let e = document.getElementsByClassName("total")[0].innerText,
    t = cartDetails.map(e => `<span>${e.qty} x ${e.name}</span>`),
    n = cartDetails.map(e => `<span>$ ${e.price * e.qty}</span>`);
  return `\n  <div class='invoice'>\n<div class='shipping-items'>\n  <div class='item-names'>${t.join("")}</div>\n  <div class='items-price'>${n.join("+")}</div>\n</div>\n  <hr>\n<div class='payment'>\n  <em>payment</em>\n  <div>\n<p>total amount to be paid:</p><span class='pay'>$ ${e}</span>\n  </div>\n</div>\n<div class='order'>\n  <button onclick='order()' class='btn-order btn'>Order Now</button>\n  <button onclick='buy(0)' class='btn-cancel btn'>Cancel</button>\n</div>\n  </div>`
}

function OrderConfirm() {
  return `\n  <div>\n<div class='order-details'>\n  <em>your order has been placed</em>\n  <p>Your order-id is : <span>${Math.round(1e3 * Math.random())}</span></p>\n  <p>your order will be delivered to you in 3-5 working days</p>\n  <p>you can pay <span>$ ${document.getElementsByClassName("total")[0].innerText}</span> by card or any online transaction method after the products have been dilivered to you</p>\n</div>\n<button onclick='okay(event)' class='btn-ok'>okay</button>\n  </div>`
}

function DisplayProducts() {
  return productDetails.map(e => Product(e)).join("")
}

function DisplayCartItems() {
  return cartDetails.map(e => CartItems(e)).join("")
}

function RenderCart() {
  document.getElementsByClassName("cart-items")[0].innerHTML = DisplayCartItems()
}

function SwitchBtns(e) {
  let t = e.getElementsByClassName("btn-add")[0];
  t.classList.toggle("qty-change");
  let n = t.classList.contains("qty-change");
  e.getElementsByClassName("btn-add")[0].innerHTML = n ? QtyBtn() : AddBtn()
}

function ToggleBackBtns() {
  let e = document.getElementsByClassName("btn-add");
  for (let t of e) t.classList.contains("qty-change") && t.classList.toggle("qty-change"), t.innerHTML = AddBtn()
}

function CartIsEmpty() {
  0 == cartDetails.length && (document.getElementsByClassName("cart-items")[0].innerHTML = "<span class='empty-cart'>Looks Like You Haven't Added Any Product In The Cart</span>")
}

function CartItemsTotal() {
  let e = cartDetails.reduce((e, t) => e + t.price * t.qty, 0),
    t = cartDetails.reduce((e, t) => e + t.qty, 0);
  document.getElementsByClassName("total")[0].innerText = e, document.getElementsByClassName("total-qty")[0].innerText = t
}

function Stocks() {
  cartDetails.forEach(e => {
    productDetails.forEach(t => {
      e.name == t.name && t.qty >= 0 && (t.qty -= e.qty, t.qty < 0 ? (t.qty += e.qty, document.getElementsByClassName("invoice")[0].style.height = "180px", document.getElementsByClassName("order-details")[0].innerHTML = "<em class='thanks'>Stocks Limit Exceeded</em>") : 0 == t.qty ? OutOfStock(t, 1) : t.qty <= 5 && OutOfStock(t, 0))
    })
  })
}

function OutOfStock(e, t) {
  let n = document.getElementsByClassName("card");
  for (let a of n) {
    let n = a.getElementsByClassName("stocks")[0],
      s = a.getElementsByClassName("product-name")[0].innerText;
    e.name == s && (t ? (a.getElementsByClassName("out-of-stock-cover")[0].style.display = "flex", n.style.display = "none") : (n.innerText = "Only Few Left", n.style.color = "orange"))
  }
}

function App() {
  return `\n  <div>\n${Banner()}\n  </div>`
}

cartDetails = [];
document.getElementById("app").innerHTML = App();

function convertString(maxLength, value) {
  if (value.length > maxLength) {
    return value.slice(0, maxLength) + "...";
  } else {
    return value;
  }
}
window.convertString = convertString;



// function renderItem(spArr) {
//   var content = "";
//   spArr.map(function (spArr, index) {
//     content += `
//     <div class="card" >
//     <div class="top-bar">
//         <i class="fab fa-apple"></i>
//         <em class="stocks">In Stock</em>
//     </div>
//     <div class="img-container">
//         <img class="product-img"
//             src="${spArr.img}"
//             alt="">
//         <div class="out-of-stock-cover"><span>Out Of Stock</span></div>
//     </div>
//     <div class="details">
//         <div class="name-fav">
//             <strong class="product-name">${spArr.name}</strong>
//             <button onclick="this.classList.toggle(&quot;fav&quot;)" class="heart"><i
//                     class="fas fa-heart"></i></button>
//         </div>
//         <div class="wrapper">
//             <h5>${spArr.name}</h5>
//             <p>${spArr.desc}</p>
//         </div>
//         <div class="purchase">
//             <p class="product-price">$${spArr.price}</p>
//             <span class="btn-add">
//                 <div>
//                     <button onclick="addItem(this)" class="add-btn">Add <i
//                             class="fas fa-chevron-right"></i></button>
//                 </div>
//             </span>
//         </div>
//     </div>
// </div>

//     `
//   });

//   getEl("mainCard").innerHTML = content;
//   // for (let index = 0; index < spArr.length; index++) {
//   //   if (index < 6) {

//   //   }

//   // }
// }


// function renderTable(spArr) {
//   var content = "";
//   spArr.map(function (spArr, index) {
//     content += `
//     <tr>
// 												<td>
// 													<div
// 														class="form-check form-check-sm form-check-custom form-check-solid">
// 														<input class="form-check-input widget-13-check" type="checkbox"
// 															value="1" />
// 													</div>
// 												</td>
// 												<td>
// 													<a href="#"
// 														class="text-dark fw-bold text-hover-primary fs-6">${spArr.id + 1}</a>
// 												</td>
// 												<td>

// 													<span
// 														class="text-dark fw-bold text-hover-primary d-block mb-1 fs-6">${spArr.name}</span>
// 												</td>
// 												<td>

// 													<span
// 														class="text-dark fw-bold text-hover-primary d-block mb-1 fs-6">${spArr.price}</span>
// 												</td>
// 												<td>

// 													<span
// 														class="text-dark fw-bold text-hover-primary d-block mb-1 fs-6">${spArr.img}</span>
// 												</td>
// 												<td class="text-dark fw-bold text-hover-primary fs-6">${spArr.desc}</td>

// 												<td class="text-end">

// 													<a href="#"
// 														class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
// 														<!--begin::Svg Icon | path: icons/duotune/art/art005.svg-->
// 														<span class="svg-icon svg-icon-3">
// 															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
// 																xmlns="http://www.w3.org/2000/svg">
// 																<path opacity="0.3"
// 																	d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
// 																	fill="currentColor" />
// 																<path
// 																	d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
// 																	fill="currentColor" />
// 															</svg>
// 														</span>
// 														<!--end::Svg Icon-->
// 													</a>
// 													<a href="#"
// 														class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm">
// 														<!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
// 														<span class="svg-icon svg-icon-3">
// 															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
// 																xmlns="http://www.w3.org/2000/svg">
// 																<path
// 																	d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z"
// 																	fill="currentColor" />
// 																<path opacity="0.5"
// 																	d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z"
// 																	fill="currentColor" />
// 																<path opacity="0.5"
// 																	d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z"
// 																	fill="currentColor" />
// 															</svg>
// 														</span>
// 														<!--end::Svg Icon-->
// 													</a>
// 												</td>
// 											</tr>
//     `
//   });

//   getEl("maintable-bodyCard").innerHTML = content;
//   // for (let index = 0; index < spArr.length; index++) {
//   //   if (index < 6) {

//   //   }

//   // }
// }
